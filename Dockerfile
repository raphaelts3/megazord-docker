FROM alpine:latest
RUN apk add --no-cache make
RUN apk add --no-cache g++
RUN apk add --no-cache libressl-dev